#![cfg_attr(test, deny(warnings))]

use clap::{self, Arg, ArgAction, ArgMatches, Command, ErrorKind};
use std::error::Error;
use std::io::{self, BufRead, Write};
use std::{cmp, env, fmt};

struct Options {
    alignstr: String,
    ign_indent: bool,
    no_skip: bool,
    _filenames: Vec<String>, //TODO: handled by options
}

struct Block {
    lines: Vec<String>,
    inds: Vec<usize>,    // byte offset of align char in each line
    offsets: Vec<usize>, // column offset of align char in each line
    max_offset: usize,
    alignstr: String,
    ign_indent: bool,
    no_skip: bool,
}

#[derive(PartialEq, Debug)]
enum Action {
    Skip,
    EndBlock,
    StartBlock,
    AddLine,
}

impl Block {
    pub fn print_aligned(&self, writer: &mut impl Write) -> io::Result<()> {
        for (i, line) in self.lines.iter().enumerate() {
            let columns = self.max_offset - self.offsets[i];
            write!(
                writer,
                "{}{}{}\n",
                line.chars().take(self.inds[i]).collect::<String>(),
                " ".repeat(columns),
                line.chars().skip(self.inds[i]).collect::<String>()
            )?;
        }
        Ok(())
    }

    pub fn get_action(&self, line: &str) -> Action {
        if !line.find(&self.alignstr).is_none() {
            if self.is_start_of_new_block(&line) {
                Action::StartBlock
            } else {
                Action::AddLine
            }
        } else if self.no_skip {
            Action::AddLine
        } else if self.lines.len() > 0 {
            Action::EndBlock
        } else {
            Action::Skip
        }
    }

    pub fn append_line(&mut self, line: &str) -> Option<()> {
        let index = line.find(&self.alignstr)?;
        self.inds.push(index);
        self.offsets.push(index);
        self.lines.push(line.to_string());
        self.max_offset = cmp::max(index, self.max_offset);
        Some(())
    }

    fn is_start_of_new_block(&self, line: &str) -> bool {
        match self.lines.last() {
            Some(block_line) => !self.ign_indent && !Block::match_indentation(block_line, line),
            None => true,
        }
    }

    fn match_indentation(line: &str, other: &str) -> bool {
        for (c1, c2) in line.chars().zip(other.chars()) {
            if c1.is_whitespace() && c2.is_whitespace() && c1 != c2 {
                return false;
            } else if !c1.is_whitespace() && !c2.is_whitespace() {
                return true;
            } else if !c1.is_whitespace() || !c2.is_whitespace() {
                return false;
            }
        }
        true
    }

    fn new(opts: Options) -> Self {
        Block {
            lines: Vec::new(),
            inds: Vec::new(),
            offsets: Vec::new(),
            max_offset: 0,
            alignstr: opts.alignstr,
            ign_indent: opts.ign_indent,
            no_skip: opts.no_skip,
        }
    }

    fn reset(&mut self) {
        self.lines = Vec::new();
        self.inds = Vec::new();
        self.offsets = Vec::new();
        self.max_offset = 0;
    }
}

impl Options {
    fn from_args(args: &ArgMatches) -> Self {
        Options {
            alignstr: args.get_one::<String>("alignstr").unwrap().to_string(),
            ign_indent: args.get_flag("ign_indent"),
            no_skip: args.get_flag("no_skip"),
            _filenames: vec!["-".into()],
        }
    }
}

struct AlignError {
    reason: String,
}

// Returning result from main calls the error's Debug
// So just map this to display to avoid e.g. "\n" showing literally
impl fmt::Debug for AlignError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.reason)
    }
}

fn main() -> Result<(), AlignError> {
    let mut stdout = io::stdout().lock();
    let stdin = io::stdin().lock();
    match align(&mut stdout, stdin, env::args()) {
        Ok(()) => Ok(()),
        Err(e) => {
            if let Some(e) = e.downcast_ref::<clap::Error>() {
                match e.kind() {
                    ErrorKind::DisplayHelp | ErrorKind::DisplayVersion => {
                        println!("{}", e.to_string());
                        Ok(())
                    }
                    _ => Err(AlignError {
                        reason: e.to_string(),
                    }),
                }
            } else {
                Err(AlignError {
                    reason: e.to_string(),
                })
            }
        }
    }
}

fn build_args<'a>() -> Command<'a> {
    Command::new("align")
        .about("Align text around a string")
        .arg(
            Arg::with_name("alignstr")
                .short('a')
                .long("align-string")
                .takes_value(true)
                .default_value(":")
                .help("The string to align text off"),
        )
        .arg(
            Arg::with_name("ign_indent")
                .short('i')
                .long("ignore-indentation")
                .action(ArgAction::SetTrue)
                .help("Ignore indentation when aligning"),
        )
        .arg(
            Arg::with_name("no_skip")
                .short('n')
                .long("no-skip")
                .action(ArgAction::SetTrue)
                .help("Don't skip non-alignable lines"),
        )
        .arg(Arg::new("filenames").multiple_values(true))
}

fn align<I>(out: &mut impl Write, input: impl BufRead, args: I) -> Result<(), Box<dyn Error>>
where
    I: IntoIterator<Item = String>,
{
    let matches = build_args().try_get_matches_from(args)?;
    let opts = Options::from_args(&matches);
    let mut block = Block::new(opts);

    for line in input.lines() {
        match line {
            Ok(line) => match block.get_action(&line) {
                Action::Skip => {
                    writeln!(out, "{}", &line)?;
                }
                Action::EndBlock => {
                    block.print_aligned(out)?;
                    block.reset();
                    writeln!(out, "{}", &line)?;
                }
                Action::StartBlock => {
                    block.print_aligned(out)?;
                    block.reset();
                    block.append_line(&line);
                }
                Action::AddLine => {
                    block.append_line(&line);
                }
            },
            Err(_) => {}
        };
    }
    if !block.lines.is_empty() {
        // maybe some trailing lines
        block.print_aligned(out)?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::{BufReader, Cursor};

    #[test]
    fn print_aligned_simple_ascii() {
        let b = Block {
            lines: vec!["foo: bar".into(), "longer: wat".into()],
            inds: vec![3, 6],
            offsets: vec![3, 6],
            max_offset: 6,
            alignstr: ":".into(),
            ign_indent: false,
            no_skip: false,
        };
        let mut writer = Cursor::new(vec![0; 20]);
        let expected = "foo   : bar\nlonger: wat\n";

        b.print_aligned(&mut writer).unwrap();

        assert_eq!(
            String::from_utf8(writer.get_ref().to_vec()).unwrap(),
            expected
        );
    }

    #[test]
    fn append_line_with_single_line_with_smaller_offset() {
        let orig_max_offset = 6;
        let line = String::from("foo: fine");
        let string_index = 3;
        let string_offset = 3;

        let mut b = Block {
            lines: vec!["foo: bar".into(), "longer: wat".into()],
            inds: vec![3, 6],
            offsets: vec![3, 6],
            max_offset: orig_max_offset,
            alignstr: ":".into(),
            ign_indent: false,
            no_skip: false,
        };
        let mut expected_lines = b.lines.to_vec();
        expected_lines.push(line.clone());

        b.append_line(&line);

        assert_eq!(b.lines, expected_lines);
        assert_eq!(b.max_offset, orig_max_offset);
        assert_eq!(b.offsets, vec![3, 6, string_offset]);
        assert_eq!(b.inds, vec![3, 6, string_index]);
    }

    #[test]
    fn append_line_with_single_line_with_larger_offset() {
        let line = String::from("longest: wow");
        let string_index = 7;
        let string_offset = 7;

        let mut b = Block {
            lines: vec!["foo: bar".into(), "longer: wat".into()],
            inds: vec![3, 6],
            offsets: vec![3, 6],
            max_offset: 6,
            alignstr: ":".into(),
            ign_indent: false,
            no_skip: false,
        };
        let mut expected_lines = b.lines.to_vec();
        expected_lines.push(line.clone());

        b.append_line(&line);

        assert_eq!(b.lines, expected_lines);
        assert_eq!(b.max_offset, string_offset);
        assert_eq!(b.offsets, vec![3, 6, string_offset]);
        assert_eq!(b.inds, vec![3, 6, string_index]);
    }

    #[test]
    fn get_action_adds_line_without_align_string_if_no_skip() {
        let b = Block {
            no_skip: true,
            // unused
            lines: vec![],
            inds: vec![],
            offsets: vec![],
            max_offset: 0,
            alignstr: ":".into(),
            ign_indent: false,
        };
        let line = String::from("No align string here");

        assert_eq!(b.get_action(&line), Action::AddLine);
    }

    #[test]
    fn get_action_skips_line_without_align_string_if_not_no_skip_and_no_lines() {
        let b = Block {
            no_skip: false,
            // unused
            lines: vec![],
            inds: vec![],
            offsets: vec![],
            max_offset: 0,
            alignstr: ":".into(),
            ign_indent: false,
        };
        let line = String::from("No align string here");

        assert_eq!(b.get_action(&line), Action::Skip);
    }

    #[test]
    fn get_action_ends_block_for_line_without_align_string_if_no_skip_and_lines() {
        let b = Block {
            no_skip: false,
            // unused
            lines: vec!["hello: there".into()],
            inds: vec![5],
            offsets: vec![5],
            max_offset: 5,
            alignstr: ":".into(),
            ign_indent: false,
        };
        let line = String::from("No align string here");

        assert_eq!(b.get_action(&line), Action::EndBlock);
    }

    #[test]
    fn get_action_starts_block_for_line_with_align_string_if_no_lines() {
        let b = Block {
            lines: vec![],
            no_skip: false,
            inds: vec![],
            offsets: vec![],
            max_offset: 0,
            alignstr: ":".into(),
            ign_indent: false,
        };
        let line = String::from("foo: bar");

        assert_eq!(b.get_action(&line), Action::StartBlock);
    }

    macro_rules! start_block_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (block_line, line, ign_indent) = $value;
                    let b = Block {
                        lines: vec![block_line.into()],
                        ign_indent: ign_indent,

                        // unused
                        offsets: vec![],
                        inds: vec![],
                        max_offset: 0,
                        alignstr: ":".into(),
                        no_skip: false,
                    };

                    assert_eq!(b.get_action(&line), Action::StartBlock);
                }
            )*
        }
    }

    start_block_tests! {
        test_simple_space_diff: ("foo: bar", " foo: bar", false),
        test_mismatch_whitespace_type: ("\tfoo: bar", " foo: bar", false),
        test_almost_matching_whitespace: ("   foo: bar", "    buz: wat", false),
    }

    macro_rules! add_line_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (block_line, line, ign_indent) = $value;
                    let b = Block {
                        lines: vec![block_line.into()],
                        ign_indent: ign_indent,

                        // unused
                        offsets: vec![],
                        inds: vec![],
                        max_offset: 0,
                        alignstr: ":".into(),
                        no_skip: false,
                    };

                    assert_eq!(b.get_action(&line), Action::AddLine);
                }
            )*
        }
    }

    add_line_tests! {
        test_matching_indent_space: (" foo: bar", " buz: wat", false),
        test_no_indent_space: ("foo: bar", "buz: wat", false),
        test_indent_at_start: (":", "wow: interesting", false),
        test_mismatch_indent_with_ignore_indent: (" foo: bar", "  buz: wat", true),
        test_only_align_str: (":", ":", false),
    }

    macro_rules! add_default_align_test {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (raw_input, expected_out) = $value;
                    let input = BufReader::new(Cursor::new(raw_input));
                    let mut out = Cursor::new(vec![0; 1024]);

                    align(&mut out, input, vec!["my-prog".to_string()]).unwrap();
                    let out_str = String::from_utf8(out.into_inner()).unwrap();

                    // trim null bytes
                    assert_eq!(out_str.trim_matches(char::from(0)), expected_out)
                }
            )*
        }
    }

    add_default_align_test! {
        test_align_single_line_no_alignstr: (
            "\
foo
bar
",
            "\
foo
bar
",
        ),
        test_align_basic_two_line_align: (
            "\
foo : bar
longer : wat
",
            "\
foo    : bar
longer : wat
",
        ),
        test_align_trailing_unalignable_line: (
            "\
foo : bar
longer : wat
some text
",
            "\
foo    : bar
longer : wat
some text
",
        ),
        test_align_trailing_separate_blocks: (
            "\
foo : bar
longer : wat
some text
foo : bar
longer : wat
",
            "\
foo    : bar
longer : wat
some text
foo    : bar
longer : wat
",
        ),
        test_align_differing_indentation_blocks: (
            "\
foo : bar
longer : wat
    foo : bar
    longer : wat
",
            "\
foo    : bar
longer : wat
    foo    : bar
    longer : wat
"
        ),
    }
}
